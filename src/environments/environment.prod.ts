export const environment = {
  production: true,
  mapbox: {
    accessToken: 'pk.eyJ1IjoiZGFueXNhbiIsImEiOiJjazNiZDdxcGowaDVtM2NsZGMycXZkc2d3In0.QNTfXPA68K68YbXzS9-H3Q'
  },
  Marker: {
    url: 'sitosanto'
  }
};
