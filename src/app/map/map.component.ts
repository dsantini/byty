import { environment } from '../../environments/environment';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
import * as mapboxgl from 'mapbox-gl';
import { DetailMarkerService } from '../home/detail-marker.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  map: mapboxgl.Map;
  style = 'mapbox://styles/mapbox/streets-v11';
  lat = 44.3532;
  lng = 11.7168;

  constructor(
    private detailMarker: DetailMarkerService,
    private router: Router
  ) {}
  ngOnInit() {
    (mapboxgl as any).accessToken = environment.mapbox.accessToken;
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: 13,
      minZoom: 12,
      center: [this.lng, this.lat],
    });
    this.map.addControl(new mapboxgl.NavigationControl());
    const geolocation = new mapboxgl.GeolocateControl({
      positionOptions: {
        enableHighAccuracy: true,
      },
      trackUserLocation: true,
    });

    this.map.addControl(geolocation);
    this.map.on('load', () => {
      this.map.resize();
      geolocation.trigger();

      this.map.addSource('fontane', {
        type: 'geojson',
        data: '../../assets/export.geojson',
        cluster: true,
        clusterMaxZoom: 13, // Max zoom to cluster points on
        clusterRadius: 50,
      });
      this.map.addLayer({
        id: 'fontane_circle',
        type: 'circle',
        source: 'fontane',
        filter: ['has', 'point_count'],
        paint: {
          'circle-color': [
            'step',
            ['get', 'point_count'],
            '#51bbd6',
            100,
            '#f1f075',
            750,
            '#f28cb1',
          ],
          'circle-radius': [
            'step',
            ['get', 'point_count'],
            20,
            100,
            30,
            750,
            40,
          ],
        },
      });

      this.map.addLayer({
        id: 'earthquake_label',
        type: 'symbol',
        source: 'fontane',
        filter: ['has', 'point_count'],
        layout: {
          'text-field': '{point_count}',
          'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
          'text-size': 10,
        },
      });

      this.map.addLayer({
        id: 'unclustered-point',
        type: 'circle',
        source: 'fontane',
        filter: ['!', ['has', 'point_count']],
        paint: {
          'circle-color': '#11b4da',

          'circle-radius': 4,
          'circle-stroke-width': 12,
          'circle-stroke-color': '#fff',
        },
      });
      this.map.on('click', 'fontane_circle', e => {
        const features = this.map.queryRenderedFeatures(e.point, {
          layers: ['fontane_circle'],
        });
        const clusterId = features[0].properties.cluster_id;
        const source = this.map.getSource('fontane');
        if (!source) {
          return;
        } else {
          source.getClusterExpansionZoom(clusterId, (err, zoom) => {
            if (err) {
              return;
            }

            this.map.easeTo({
              center: features[0].geometry.coordinates,
              zoom,
            });
          });
        }
      });
      this.map.on('click', 'unclustered-point', e => {
        this.router.navigate(['./home/', e.id.toString()]);
      });
      this.detailMarker.getAllMarker().forEach(e => {
        // const el = document.createElement('div');
        // el.innerHTML = '<img src="../../assets/Fountain_icon.svg"/>';
        // el.style.width = '30px';
        // el.className = 'marker';
        // const t = new mapboxgl.Marker(el)
        //   .setLngLat([e.lon, e.lat])
        //   .addTo(this.map);
        // t.getElement().addEventListener('click', () => {
        //   this.router.navigate(['./home/', e.id.toString()]);
        // });
      });
    });
  }
}
