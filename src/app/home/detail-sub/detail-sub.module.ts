import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailSubPageRoutingModule } from './detail-sub-routing.module';

import { DetailSubPage } from './detail-sub.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailSubPageRoutingModule
  ],
  declarations: [DetailSubPage]
})
export class DetailSubPageModule {}
