import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailSubPage } from './detail-sub.page';

describe('DetailSubPage', () => {
  let component: DetailSubPage;
  let fixture: ComponentFixture<DetailSubPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailSubPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailSubPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
