import { TestBed } from '@angular/core/testing';

import { DetailMarkerService } from './detail-marker.service';

describe('DetailMarkerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetailMarkerService = TestBed.get(DetailMarkerService);
    expect(service).toBeTruthy();
  });
});
